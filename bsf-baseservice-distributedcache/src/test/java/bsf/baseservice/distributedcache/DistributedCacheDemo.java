package bsf.baseservice.distributedcache;

import bsf.base.CallBack;
import bsf.base.TimeSpan;
import bsf.util.ExceptionUtil;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;

/**
 * Created by chejiangyi on 2017/4/20.
 */

public class DistributedCacheDemo {
    public static final void main(String[] args) {
        DistributedCacheDemo demo = new DistributedCacheDemo();
        demo.run();
    }

    public final void run() {
        for (int i = 0; i < 10; i++) {
            final int a = i;
            System.out.println(i + "次获取");
            //获取某业务缓存的业务1
            new TestCacheHelper().getYeWu1(330100, TestCache1.class, new CallBack.Func0<TestCache1>() {
                        @Override
                        public TestCache1 invoke() {
                            TestCache1 t = new TestCache1();
                            t.data = "THIS IS TEST ENTITY!!!";
                            System.out.println(a + "次实时查询");
                            return t;
                        }
                    }
            );
            try {
                Thread.sleep(1000 * 2);
            } catch (Exception exp) {
            }
        }
        //删除某业务的业务1的key缓存
        new TestCacheHelper().deleteYeWu1Cache(330100);
    }

    //
//     * 某个业务需要的缓存信息 建议为业务名+Cache
//
    public static class TestCache1 {
        public String data;
        public int data2;
    }

    /**
     * 定义一个业务缓存帮助类
     * 建议格式:业务名+CacheHelper
     * 这个类只是个例子，具体要根据实际业务情况而定，要思考而定，不要整块拷贝
     */
    public final class TestCacheHelper {
        private String Key = "DydTest"; //定义该业务的唯一标示，建议为业务名

        private java.util.ArrayList<String> getConfigs() {
//            //借助"配置中心"动态设置负载均衡示例
//            java.util.ArrayList<String> configs = new java.util.ArrayList<String>();
//            for (int i = 0; i < 100; i++) //假设最大支持100个cache负载均衡
//            {
//                var config = ConfigManager.ConfigManagerHelper.<String>Get(Key + i); //判断第n个负载均衡是否存在
//                if (DotNetToJavaStringHelper.isNullOrEmpty(config))
//                {
//                    break;
//                }
//                configs.add(config); //若存在则添加入负载均衡列表
//            }
//            return configs;
//
//            * 分布式缓存连接字符串配置格式说明
//            * 格式:底层存储;指定底层存储的配置连接字符串;
//            * 举例:type=redis;host=192.168.1.128;port=6379;password=;maxpool=20;minpool=20;
//            * （;分隔信息）
//            * 目前支持的底层存储:redis,ssdb,aliyunmemcached;未来支持:ssdb,memcached,sqlserver
//            * redis连接格式说明:host(表示ip)port:(端口),password(表示密码),maxpool(表示最大连接池大小),minpool(表示最小连接池大小)
//            * ssdb连接格式说明:暂不支持
//            * aliyunmemcached连接格式说明（未测试，只是写了代码）:networkaddress(表示阿里云内网地址),port,zone(未知参数),username,password,minpoolsize(表示最小连接数),maxpoolsize(表示最大连接池数)
//
            ArrayList<String> rs = new ArrayList<>();
            rs.add("type=redis;host=192.168.1.128;port=6379;");
            return rs;
        }

        private String getKey(String testkey) {
            return Key + "-" + testkey; //缓存到底层存储的key
        }

        public final <T> T getYeWu1(int shopid, Class<T> cls, CallBack.Func0<T> action) {
            String isopencache = "true";//ConfigManager.ConfigManagerHelper.<String>Get(Key + "_Yewu1"); //配置中心集成判断业务是否开启缓存
            if (isopencache != null && isopencache.toLowerCase().equals("true")) {
                String exprietime = "3"; //ConfigManagerHelper.<String>Get(Key + "_Yewu1_exprietime"); //配置中心集成业务缓存时间
                int cacheexprieseconds = (StringUtils.isEmpty(exprietime) ? 30 : Integer.parseInt(exprietime));

                return DistributedCacheHelper.getOrSetValue(this.getConfigs(), this.getKey(shopid + ""), new TimeSpan(cacheexprieseconds * 1000), cls, action);
            }
            return null;
        }

        public final void deleteYeWu1Cache(int shopid) {
            DistributedCacheHelper.delete(this.getConfigs(), new String[]{this.getKey(shopid + "")});
        }
    }


}