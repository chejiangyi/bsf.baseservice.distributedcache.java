package bsf.baseservice.distributedcache.storage.aliyunmemcached;

import bsf.baseservice.distributedcache.storage.BaseConfig;

public class AliyunMemcachedCacheConfig extends BaseConfig
{
	/** 
	 阿里云实例ID

	*/
	public String NetworkAddress;
	public int Port;
	public String Zone = "";
	public String UserName = "";
	public String Password = "";

	/**
	 超时（毫秒）

	*/
	public long ConnectionTimeout = 1000;
	/**
	 最小缓存池大小

	*/
	public int MinPoolSize = 5;
	/**
	 最大缓存池大小
	 
	*/
	public int MaxPoolSize = 200;


	@Override
	public boolean equals(Object obj)
	{
		if (obj.hashCode() == this.hashCode())
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	@Override
	public int hashCode()
	{
		return (NetworkAddress + "_" + Port).hashCode();
	}

}