package bsf.baseservice.distributedcache.storage;

import bsf.base.TimeSpan;

public class BaseCache implements AutoCloseable
{
	public BaseConfig Config;
	protected String cacheKey;

	public <T> T getValue(Class<T> cls)
	{
		return null;
	}

	public <T> boolean setValue(T value, TimeSpan expiretime)
	{
		return false;
	}

	public void openConn(String key)
	{

	}

	public void delete()
	{
	}

	public void close()
	{

	}
}