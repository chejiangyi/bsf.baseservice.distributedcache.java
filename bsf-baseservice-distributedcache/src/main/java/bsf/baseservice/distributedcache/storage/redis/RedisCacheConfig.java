package bsf.baseservice.distributedcache.storage.redis;

import bsf.baseservice.distributedcache.storage.BaseConfig;

public class RedisCacheConfig extends BaseConfig
{
	public String Host="";
	public int Port = 6379;
	public String Password = null;
	public int MaxPool = 100;
	public int MinPool = 10;
}