package bsf.baseservice.distributedcache.systemruntime;

import bsf.base.BsfException;

public class DistributedCacheException extends BsfException
{
	public DistributedCacheException(String message)
	{
		super(message);

	}

	public DistributedCacheException(String message,Throwable exp)
	{
		super(message,exp);

	}
}