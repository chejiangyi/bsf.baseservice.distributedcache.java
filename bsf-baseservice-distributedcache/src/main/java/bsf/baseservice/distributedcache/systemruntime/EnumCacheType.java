package bsf.baseservice.distributedcache.systemruntime;

public enum EnumCacheType
{
	/** 
	 Redis 
	 数据存内存,适合内存大小范围内大量缓存。（若是频繁失效的缓存数据，大量热点数据，建议使用redis）

	*/
	Redis,
	/**
	 SSDB
	 数据热点存内存，大量数据存磁盘。（若是命中率较低，命中热点数据，大量冷数据，建议使用ssdb）

	*/
	SSDB,
	/**
	 Memcached

	*/
	Memcached,
	/**
	 SQLServer内存表

	*/
	SqlServer,
	/**
	 阿里云的缓存服务OCS
	 
	*/
	AliyunMemcached;

}