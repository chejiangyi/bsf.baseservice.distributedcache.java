package bsf.baseservice.distributedcache.systemruntime;


public class DistributedCacheSerializationException extends DistributedCacheException
{
	public DistributedCacheSerializationException(Class type, Throwable exp)
	{
		super(String.format("分布式缓存序列化/反序列化失败,类型为:%1$s",type.getName()), exp);

	}

	public DistributedCacheSerializationException(String json, Throwable exp)
	{
		super(String.format("分布式缓存序列化/反序列化失败,序列化内容为:%1$s", json), exp);

	}

	public DistributedCacheSerializationException(Throwable exp)
	{
		super("分布式缓存序列化/反序列化失败", exp);

	}
}