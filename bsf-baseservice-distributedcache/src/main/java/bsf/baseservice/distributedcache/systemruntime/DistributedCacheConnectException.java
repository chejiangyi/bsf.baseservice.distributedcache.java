package bsf.baseservice.distributedcache.systemruntime;


import bsf.util.StringUtil;

public class DistributedCacheConnectException extends DistributedCacheException
{
	public DistributedCacheConnectException(String message)
	{
		super(String.format("分布式缓存连接失败,请检查连接是否可用;错误信息:%1$", StringUtil.nullToEmpty(message)));

	}

	public DistributedCacheConnectException(String message, Exception exp)
	{
		super(String.format("分布式缓存连接失败,请检查连接是否可用;错误信息:%1$s", StringUtil.nullToEmpty(message), exp));

	}
}