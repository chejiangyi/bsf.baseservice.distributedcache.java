package bsf.baseservice.distributedcache.loadbalance;


public interface IBalance
{
	String chooseServer(java.util.ArrayList<String> serviceconfiglist, String key);
}