package bsf.baseservice.distributedcache.loadbalance;


public class HashBalance implements IBalance
{
	public String chooseServer(java.util.ArrayList<String> serviceconfiglist, String key)
	{
		if (serviceconfiglist == null||serviceconfiglist.isEmpty())
		{
			return null;
		}
		return serviceconfiglist.get(Math.abs(key.hashCode()) % serviceconfiglist.size());
	}
}