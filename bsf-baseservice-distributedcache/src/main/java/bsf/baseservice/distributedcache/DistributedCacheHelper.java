package bsf.baseservice.distributedcache;

import bsf.base.CallBack;
import bsf.baseservice.distributedcache.storage.CacheFactory;
import bsf.base.TimeSpan;

/**
 分布式缓存帮助类
 用于兼容不同版本及简化使用

*/
public class DistributedCacheHelper
{
//
//         * 分布式缓存连接字符串配置格式说明
//         * 格式:底层存储;指定底层存储的配置连接字符串;
//         * 举例:type=redis;host=192.168.17.54:6379;password=;maxwritepool=20;maxreadpool=20;
//         * （;分隔信息）
//         * 目前支持的底层存储:redis;未来支持:ssdb,memcached,sqlserver,阿里云缓存服务
//         * redis连接格式说明:host(表示ip:端口),password(表示密码),maxwritepool(表示最大写连接池大小),maxreadpool(表示最大读连接池大小)
//         *
//

	/**
	 获取或设置key的缓存,并在制定时间后失效

	 <typeparam name="T">数据集类型</typeparam>
	 @param serverconfigs 分布式缓存连接字符串配置（负载均衡需要配置多个）
	 @param key
	 @param expiretime 过期时间
	 @param action 缓存的数据集回调 （当缓存失效的时候或者第一次初始化将回调此方法获取最新数据）
	 @return 返回数据集
	*/
	public static <T> T getOrSetValue(java.util.ArrayList<String> serverconfigs, String key, TimeSpan expiretime, Class<T> cls, CallBack.Func0 action)
	{
		return (T) CacheFactory.getOrSetValue(serverconfigs, key, expiretime,cls, action);
	}

	/**
	 获取缓存

	 <typeparam name="T">数据集类型</typeparam>
	 @param serverconfigs 分布式缓存连接字符串配置（负载均衡需要配置多个）
	 @param key
	 @return 返回数据集
	*/
	public static <T> T getValue(java.util.ArrayList<String> serverconfigs, String key,Class<T> cls)
	{
		return CacheFactory.getValue(serverconfigs, key,cls);
	}

	/**
	 删除Key
	 
	 @param serverconfigs
	 @param keys
	*/
	public static void delete(java.util.ArrayList<String> serverconfigs, String[] keys)
	{
		CacheFactory.delete(serverconfigs, keys);
	}
}